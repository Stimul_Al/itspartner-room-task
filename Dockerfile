FROM openjdk:11

ADD target/*.jar /app_room.jar

ENTRYPOINT ["java", "-jar", "app_room.jar"]
