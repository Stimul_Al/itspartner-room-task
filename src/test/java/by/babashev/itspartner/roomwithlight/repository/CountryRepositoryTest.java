package by.babashev.itspartner.roomwithlight.repository;

import by.babashev.itspartner.roomwithlight.entity.Country;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CountryRepositoryTest {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private RoomRepository roomRepository;

    @BeforeEach
    void cleanUp() {
        roomRepository.deleteAll();
        countryRepository.deleteAll();
    }

    @Test
    void findByName_happyPath() {
        //given
        Country countryToSave = Country.builder()
                .name("Island")
                .ip("15.15.15.15")
                .build();

        Country savedCountry = countryRepository.save(countryToSave);

        //when
        Country foundCountry = countryRepository.findByName(savedCountry.getName());

        //then
        assertNotNull(foundCountry);
        assertEquals(foundCountry.getId(), savedCountry.getId());
        assertEquals(foundCountry.getName(), savedCountry.getName());
        assertEquals(foundCountry.getIp(), savedCountry.getIp());
    }
}
