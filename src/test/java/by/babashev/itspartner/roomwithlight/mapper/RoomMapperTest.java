package by.babashev.itspartner.roomwithlight.mapper;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import by.babashev.itspartner.roomwithlight.entity.Country;
import by.babashev.itspartner.roomwithlight.entity.Room;
import by.babashev.itspartner.roomwithlight.entity.enums.StateLight;
import by.babashev.itspartner.roomwithlight.repository.CountryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static by.babashev.itspartner.roomwithlight.mapper.RoomMapper.ROOM_MAPPER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@SpringBootTest
public class RoomMapperTest {

    @Autowired
    private CountryRepository countryRepository;

    @Test
    void shouldMapEntity() {
        //given
        RoomCreateDto roomCreateDto = new RoomCreateDto();
        roomCreateDto.setName("Party");
        roomCreateDto.setCountry("Belarus");

        //when
        Room room = ROOM_MAPPER.mapToEntity(roomCreateDto);

        //then
        assertNull(room.getCountry());
        assertNull(room.getStateLight());
        assertEquals(room.getName(), roomCreateDto.getName());
    }

    @Test
    void shouldMapFullDto() {
        //given
        Country country = countryRepository.findByName("Belarus");

        Room room = Room.builder()
                .id(1L)
                .name("Party")
                .country(country)
                .stateLight(StateLight.OFF)
                .build();

        //when
        RoomFullDto roomFullDto = ROOM_MAPPER.mapToFullDto(room);

        //then
        Assertions.assertNotNull(roomFullDto);
        Assertions.assertEquals(roomFullDto.getId(), room.getId());
        Assertions.assertEquals(roomFullDto.getName(), room.getName());
    }

}
