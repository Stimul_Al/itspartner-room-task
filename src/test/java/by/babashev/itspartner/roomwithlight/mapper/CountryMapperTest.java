package by.babashev.itspartner.roomwithlight.mapper;

import by.babashev.itspartner.roomwithlight.dto.country.CountryPreviewDto;
import by.babashev.itspartner.roomwithlight.entity.Country;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CountryMapperTest {

    @Test
    void shouldMapToPreviewDto() {
        //given
        Country country = Country.builder()
                .id(1L)
                .name("Belarus")
                .build();

        //when
        CountryPreviewDto countryPreviewDto = CountryMapper.COUNTRY_MAPPER.mapToPreviewDto(country);

        //then
        Assertions.assertNotNull(countryPreviewDto);
        Assertions.assertEquals(countryPreviewDto.getName(), country.getName());
    }
}
