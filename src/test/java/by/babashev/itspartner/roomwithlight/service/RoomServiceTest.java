package by.babashev.itspartner.roomwithlight.service;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import by.babashev.itspartner.roomwithlight.entity.Country;
import by.babashev.itspartner.roomwithlight.entity.Room;
import by.babashev.itspartner.roomwithlight.repository.CountryRepository;
import by.babashev.itspartner.roomwithlight.repository.CountryRepositoryTest;
import by.babashev.itspartner.roomwithlight.repository.RoomRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class RoomServiceTest {

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private CountryRepository countryRepository;

    @BeforeEach
    void cleanUp() {
        roomRepository.deleteAll();
        countryRepository.deleteAll();
    }

    @Test
    void create_happyPath() {
        //given
        Country savedCountry = countryRepository
                .save(Country.builder()
                        .name("Island")
                        .ip("15.15.15.15")
                        .build());

        RoomCreateDto roomToSave = new RoomCreateDto();
        roomToSave.setName("Island man");
        roomToSave.setCountry("Island");

        //when
        RoomFullDto savedRoom = roomService.create(roomToSave);

        //then
        assertNotNull(savedRoom.getId());
        assertEquals(savedRoom.getCountry().getName(), roomToSave.getCountry());
        assertEquals(savedRoom.getName(), roomToSave.getName());
    }

}
