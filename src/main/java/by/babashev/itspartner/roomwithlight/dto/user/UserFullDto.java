package by.babashev.itspartner.roomwithlight.dto.user;

import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import lombok.Data;

@Data
public class UserFullDto {

    private Long id;
    private String name;
    private String ip;
    private RoomFullDto room;
}
