package by.babashev.itspartner.roomwithlight.dto.room;

import by.babashev.itspartner.roomwithlight.dto.user.UserPreviewDto;
import by.babashev.itspartner.roomwithlight.entity.Country;
import by.babashev.itspartner.roomwithlight.entity.enums.StateLight;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RoomFullDto {

    private Long id;
    private String name;
    private StateLight stateLight;
    private Country country;
    private List<UserPreviewDto> users = new ArrayList<>();
}
