package by.babashev.itspartner.roomwithlight.dto.user;

import lombok.Data;

@Data
public class UserPreviewDto {

    private Long id;
    private String name;
}
