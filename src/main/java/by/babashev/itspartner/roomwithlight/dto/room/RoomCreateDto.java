package by.babashev.itspartner.roomwithlight.dto.room;

import lombok.Data;
import javax.validation.constraints.NotBlank;

@Data
public class RoomCreateDto {

    @NotBlank
    private String name;

    @NotBlank
    private String country;
}
