package by.babashev.itspartner.roomwithlight.dto.country;

import lombok.Data;

@Data
public class CountryPreviewDto {

    private Long id;
    private String name;
    private String ip;
}
