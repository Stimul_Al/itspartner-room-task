package by.babashev.itspartner.roomwithlight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomWithLightApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoomWithLightApplication.class, args);
	}

}
