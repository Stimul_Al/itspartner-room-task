package by.babashev.itspartner.roomwithlight.service;

import by.babashev.itspartner.roomwithlight.dto.country.CountryPreviewDto;
import by.babashev.itspartner.roomwithlight.entity.Country;

import java.util.List;

public interface CountryService {

    CountryPreviewDto findById(Long id);

    List<CountryPreviewDto> findAll();

}
