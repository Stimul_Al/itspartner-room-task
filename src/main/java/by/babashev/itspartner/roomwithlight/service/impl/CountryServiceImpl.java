package by.babashev.itspartner.roomwithlight.service.impl;

import by.babashev.itspartner.roomwithlight.dto.country.CountryPreviewDto;
import by.babashev.itspartner.roomwithlight.exception.EntityNotFoundException;
import by.babashev.itspartner.roomwithlight.repository.CountryRepository;
import by.babashev.itspartner.roomwithlight.service.CountryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.itspartner.roomwithlight.mapper.CountryMapper.COUNTRY_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Override
    @Transactional(readOnly = true)
    public CountryPreviewDto findById(Long id) {
        CountryPreviewDto country = countryRepository.findById(id)
                .map(COUNTRY_MAPPER::mapToPreviewDto)
                .orElseThrow(() -> new EntityNotFoundException("Country not found"));

        log.info("CountryService impl -> find country by id: {}", id);
        return country;
    }

    @Override
    @Transactional(readOnly = true)
    public List<CountryPreviewDto> findAll() {
        List<CountryPreviewDto> countries = countryRepository.findAll()
                .stream()
                .map(COUNTRY_MAPPER::mapToPreviewDto)
                .collect(Collectors.toList());

        log.info("CountryService impl -> find countries: {}", countries.size());
        return countries;
    }
}
