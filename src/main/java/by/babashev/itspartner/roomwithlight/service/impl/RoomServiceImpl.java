package by.babashev.itspartner.roomwithlight.service.impl;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import by.babashev.itspartner.roomwithlight.entity.Room;
import by.babashev.itspartner.roomwithlight.entity.enums.StateLight;
import by.babashev.itspartner.roomwithlight.exception.EntityNotFoundException;
import by.babashev.itspartner.roomwithlight.repository.CountryRepository;
import by.babashev.itspartner.roomwithlight.repository.RoomRepository;
import by.babashev.itspartner.roomwithlight.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.itspartner.roomwithlight.mapper.RoomMapper.ROOM_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final CountryRepository countryRepository;

    @Override
    @Transactional(readOnly = true)
    public RoomFullDto findById(Long id) {
        RoomFullDto room = roomRepository.findById(id)
                .map(ROOM_MAPPER::mapToFullDto)
                .orElseThrow(() -> new EntityNotFoundException("Room not found"));

        log.info("RoomServiceImpl -> room found by id: {}", id);
        return room;
    }

    @Override
    @Transactional(readOnly = true)
    public List<RoomFullDto> findAll() {
        List<RoomFullDto> rooms = roomRepository.findAll()
                .stream()
                .map(ROOM_MAPPER::mapToFullDto)
                .collect(Collectors.toList());

        log.info("RoomServiceImpl -> rooms found: {}", rooms.size());
        return rooms;
    }

    @Override
    @Transactional
    public RoomFullDto create(RoomCreateDto createDto) {
        Room room = ROOM_MAPPER.mapToEntity(createDto);
        room.setCountry(countryRepository.findByName(createDto.getCountry()));
        room.setStateLight(StateLight.OFF);

        Room savedRoom = roomRepository.save(room);

        log.info("RoomServiceImpl -> room create: {}", savedRoom.getId());
        return ROOM_MAPPER.mapToFullDto(savedRoom);
    }

    @Override
    @Transactional
    public RoomFullDto update(Long id) {
        Room room = roomRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("not found entity to update"));

        if(!room.getStateLight().getState()) {
            room.setStateLight(StateLight.ON);
        } else {
            room.setStateLight(StateLight.OFF);
        }

        log.info("RoomServiceImpl -> room update: {}", room.getId());
        return ROOM_MAPPER.mapToFullDto(roomRepository.save(room));
    }
}
