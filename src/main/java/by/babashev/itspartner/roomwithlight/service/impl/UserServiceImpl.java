package by.babashev.itspartner.roomwithlight.service.impl;

import by.babashev.itspartner.roomwithlight.dto.user.UserFullDto;
import by.babashev.itspartner.roomwithlight.dto.user.UserPreviewDto;
import by.babashev.itspartner.roomwithlight.exception.EntityNotFoundException;
import by.babashev.itspartner.roomwithlight.repository.UserRepository;
import by.babashev.itspartner.roomwithlight.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static by.babashev.itspartner.roomwithlight.mapper.UserMapper.USER_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Long id) {
        UserFullDto user = userRepository.findById(id)
                .map(USER_MAPPER::mapToFullDto)
                .orElseThrow(() -> new EntityNotFoundException("User not found"));

        log.info("UserServiceImpl -> found user by id: {}", id);
        return user;
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserPreviewDto> findAll() {
        List<UserPreviewDto> users = userRepository.findAll()
                .stream()
                .map(USER_MAPPER::mapToPreviewDto)
                .collect(Collectors.toList());

        log.info("UserServiceImpl -> found users: {}", users.size());
        return users;
    }
}
