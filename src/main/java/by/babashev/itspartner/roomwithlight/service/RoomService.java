package by.babashev.itspartner.roomwithlight.service;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;

import java.util.List;

public interface RoomService {

    RoomFullDto findById(Long id);

    List<RoomFullDto> findAll();

    RoomFullDto create(RoomCreateDto createDto);

    RoomFullDto update(Long id);

}
