package by.babashev.itspartner.roomwithlight.service;

import by.babashev.itspartner.roomwithlight.dto.user.UserFullDto;
import by.babashev.itspartner.roomwithlight.dto.user.UserPreviewDto;

import java.util.List;

public interface UserService {

    UserFullDto findById(Long id);

    List<UserPreviewDto> findAll();
}
