package by.babashev.itspartner.roomwithlight.entity.enums;

public enum StateLight {

    ON(true),
    OFF(false);

    private boolean state;

    StateLight(Boolean state) {
        this.state = state;
    }

    public boolean getState(){
        return state;
    }
}
