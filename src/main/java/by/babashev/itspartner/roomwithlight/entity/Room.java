package by.babashev.itspartner.roomwithlight.entity;

import by.babashev.itspartner.roomwithlight.entity.enums.StateLight;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "rooms")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "state_light", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private StateLight stateLight;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;
}
