package by.babashev.itspartner.roomwithlight.mapper;

import by.babashev.itspartner.roomwithlight.dto.country.CountryPreviewDto;
import by.babashev.itspartner.roomwithlight.entity.Country;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CountryMapper {

    CountryMapper COUNTRY_MAPPER = Mappers.getMapper(CountryMapper.class);

    CountryPreviewDto mapToPreviewDto(Country country);

    List<CountryPreviewDto> mapToListPreviewDto(List<Country> country);
}
