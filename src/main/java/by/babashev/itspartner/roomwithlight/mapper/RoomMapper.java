package by.babashev.itspartner.roomwithlight.mapper;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import by.babashev.itspartner.roomwithlight.entity.Room;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = CountryMapper.class)
public interface RoomMapper {

    RoomMapper ROOM_MAPPER = Mappers.getMapper(RoomMapper.class);

    RoomFullDto mapToFullDto(Room room);

    @Mapping(target = "country", ignore = true)
    Room mapToEntity(RoomCreateDto roomCreateDto);

    List<RoomFullDto> mapToListFullDto(List<Room> rooms);
}
