package by.babashev.itspartner.roomwithlight.mapper;

import by.babashev.itspartner.roomwithlight.dto.user.UserFullDto;
import by.babashev.itspartner.roomwithlight.dto.user.UserPreviewDto;
import by.babashev.itspartner.roomwithlight.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    UserFullDto mapToFullDto(User user);

    UserPreviewDto mapToPreviewDto(User user);
}
