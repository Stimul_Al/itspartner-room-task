package by.babashev.itspartner.roomwithlight.repository;

import by.babashev.itspartner.roomwithlight.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
