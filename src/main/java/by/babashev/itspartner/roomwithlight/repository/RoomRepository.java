package by.babashev.itspartner.roomwithlight.repository;

import by.babashev.itspartner.roomwithlight.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
