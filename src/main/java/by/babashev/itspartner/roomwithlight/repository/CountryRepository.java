package by.babashev.itspartner.roomwithlight.repository;

import by.babashev.itspartner.roomwithlight.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CountryRepository extends JpaRepository<Country, Long> {

    @Query(value = "SELECT * FROM countries c WHERE c.name = :name", nativeQuery = true)
    Country findByName(@Param("name") String name);
}
