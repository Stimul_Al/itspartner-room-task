package by.babashev.itspartner.roomwithlight.controller;

import by.babashev.itspartner.roomwithlight.dto.room.RoomCreateDto;
import by.babashev.itspartner.roomwithlight.dto.room.RoomFullDto;
import by.babashev.itspartner.roomwithlight.service.CountryService;
import by.babashev.itspartner.roomwithlight.service.RoomService;
import by.babashev.itspartner.roomwithlight.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class RoomController {
    
    private final RoomService roomService;
    private final CountryService countryService;
    private final UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/index")
    public String openMainPage(Model model) {

        model.addAttribute("rooms", roomService.findAll());
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/room/{id}")
    public String openRoom(@PathVariable Long id, Model model) {
        RoomFullDto foundRoom = roomService.findById(id);
        model.addAttribute("room", foundRoom);

        return "room";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/room")
    public String createRoom(Model model) {
        model.addAttribute("createDto", new RoomCreateDto());

        return "create-room";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/room/create")
    public String createRoom(RoomCreateDto createDto) {
        RoomFullDto newRoom = roomService.create(createDto);

        return "redirect:/room/" + newRoom.getId();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/room/update/{id}")
    public String updateRoom(@PathVariable Long id) {
        System.out.println("Вызвали update");
        roomService.update(id);

        System.out.println("Выполнили update");
        return "redirect:/room/" + id;
    }
}
